<?php

namespace Drupal\cloud_orchestrator\Installer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Additional Cloud Orchestrator related configurations.
 */
class ModuleConfigureForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloud_orchestrator_module_configure_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#title'] = 'Additional configurations';

    $form['cloud_service_providers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cloud Service Providers'),
      '#options' => [
        'aws_cloud' => 'AWS Cloud',
        'k8s' => 'Kubernetes',
      ],
      '#description' => $this->t('Choose the cloud service provider to enable.'),
      '#required' => TRUE,
    ];

    $form['install_ldap'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Install LDAP modules'),
      '#description' => $this->t('Install LDAP modules to authenticate with Active Directory.  LDAP can also be installed at a later time.'),
    ];

    $form['actions']['submit']['#value'] = $this->t('Save and continue');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $installer */
    $installer = \Drupal::service('module_installer');

    if ($form_state->getValue('install_ldap') == TRUE) {
      // Install the LDAP modules.
      $installer->install([
        'ldap_user',
        'ldap_servers',
        'ldap_authorization',
        'ldap_query',
        'ldap_authorization',
        'ldap_authentication',
        'authorization',
        'externalauth',
      ], TRUE);
    }

    $cloud_providers = array_values(array_filter($form_state->getValue('cloud_service_providers')));

    // Both providers are selected.  Enable both of them.
    if (count($cloud_providers) == 2) {
      $installer->install([
        'aws_cloud',
        'k8s',
        'geocoder',
      ]);

      // Add the dashboard pages.
      $files = [
        'page_manager.page_variant.dashboard-panels_variant-0.yml',
        'page_manager.page_variant.kubernetes_dashboard-panels_variant-0.yml',
        'page_manager.page_variant.all_dashboard-panels_variant-0.yml',
      ];
      cloud_orchestrator_update_yml_definitions($files, 'optional');

      \Drupal::configFactory()
        ->getEditable('aws_cloud.settings')
        ->set('aws_cloud_instance_type_prices_spreadsheet', FALSE)
        ->save();
      // Set the homepage.
      \Drupal::configFactory()
        ->getEditable('system.site')
        ->set('page.front', '/all-cloud-dashboard')
        ->save(TRUE);
    }
    else {
      $provider = array_shift($cloud_providers);
      switch ($provider) {
        case 'aws_cloud':
          $installer->install([
            'aws_cloud',
          ]);
          // Add the dashboard page.
          cloud_orchestrator_update_yml_definitions(['page_manager.page_variant.dashboard-panels_variant-0.yml'], 'optional');

          \Drupal::configFactory()
            ->getEditable('aws_cloud.settings')
            ->set('aws_cloud_instance_type_prices_spreadsheet', FALSE)
            ->save();
          \Drupal::configFactory()
            ->getEditable('system.site')
            ->set('page.front', '/dashboard')
            ->save(TRUE);

          break;

        case 'k8s':
          $installer->install([
            'k8s',
            'geocoder',
          ]);
          cloud_orchestrator_update_yml_definitions(['page_manager.page_variant.kubernetes_dashboard-panels_variant-0.yml'], 'optional');
          \Drupal::configFactory()
            ->getEditable('system.site')
            ->set('page.front', '/k8s-dashboard')
            ->save(TRUE);

          break;

        default:
          break;
      }
    }

    // We install some menu links, so we have to rebuild the router,
    // to ensure the menu links are valid.
    \Drupal::service('router.builder')->rebuildIfNeeded();
    drupal_flush_all_caches();

  }

}
