<?php

namespace Drupal\cloud_orchestrator\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;

/**
 * Class for Tabbed layout.
 */
class TwoColumnTabbedLayout extends LayoutDefault {

  /**
   * Number of tabs supported in this layout.
   *
   * @var int
   */
  private $tabCount = 2;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    // Generate configuration depending the number of tabs.
    for ($i = 0; $i < $this->tabCount; $i++) {
      $config[$this->getTabKey($i)] = '';
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $i = 0;
    for ($i = 0, $num = 1; $i < $this->tabCount; $i++, $num++) {
      $label = $this->getTabKey($i);
      $form[$label] = [
        '#type' => 'textfield',
        '#title' => $this->t("Tab @num Label", ['@num' => $num]),
        '#default_value' => $this->configuration[$label],
        '#description' => $this->t('Label to display for this tab.'),
      ];
      if ($i === 0) {
        $form[$label]['#required'] = TRUE;
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    for ($i = 0; $i < $this->tabCount; $i++) {
      $label = $this->getTabKey($i);
      $this->configuration[$label] = $form_state->getValue($label);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    $build['#tab_count'] = $this->tabCount;
    for ($i = 0; $i < $this->tabCount; $i++) {
      $label = $this->getTabKey($i);
      if (!empty($this->configuration[$label])) {
        $build['#tab_labels'][] = $this->configuration[$label];
      }
    }
    return $build;
  }

  /**
   * Get the tab label value.
   *
   * @param int $num
   *   The number to generate the label for.
   *
   * @return string
   *   The label for a particular tab.
   */
  private function getTabKey($num) {
    return "tab_{$num}_label";
  }

}
