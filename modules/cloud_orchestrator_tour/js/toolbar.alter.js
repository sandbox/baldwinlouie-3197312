(function ($, Drupal, drupalSettings) {
  var escapeAdminPath = sessionStorage.getItem('escapeAdminPath');
  if (escapeAdminPath.indexOf('?tour') != -1) {
    var split = escapeAdminPath.split("?tour");
    if (split.length == 2) {
      sessionStorage.setItem('escapeAdminPath', split[0]);
      console.log(split[0]);
    }
  }
})(jQuery, Drupal, drupalSettings);
