<?php

/**
 * @file
 * Cloud Orchestrator Installer.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Yaml;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function cloud_orchestrator_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Add a placeholder as example that one can choose an arbitrary site name.
  $form['site_information']['site_name']['#default_value'] = t('Cloud Orchestrator');
}

/**
 * Implements hook_install_tasks().
 */
function cloud_orchestrator_install_tasks() {
  $tasks = [
    'cloud_orchestrator_module_configure_form' => [
      'display_name' => t('Additional configurations'),
      'type' => 'form',
      'function' => 'Drupal\cloud_orchestrator\Installer\Form\ModuleConfigureForm',
    ],
  ];
  return $tasks;
}

/**
 * Implements hook_modules_uninstalled().
 */
function cloud_orchestrator_modules_uninstalled($modules) {
  // Update the homepage when k8s and aws_cloud modules
  // are being disabled.
  $module_list = Drupal::moduleHandler()->getModuleList();
  $front_page = '/clouds';

  // If both modules are being disabled, set homepage to /clouds.
  if (in_array('k8s', $modules) && in_array('aws_cloud', $modules)) {
    $front_page = '/clouds';
  }
  elseif (in_array('k8s', $modules)) {
    if (!empty($module_list['aws_cloud'])) {
      $front_page = '/dashboard';
    }
  }
  elseif (in_array('aws_cloud', $modules)) {
    if (!empty($module_list['k8s'])) {
      $front_page = '/k8s-dashboard';
    }
  }
  elseif (in_array('cloud', $modules)) {
    // If cloud module is disabled. Set to the static welcome page.
    $front_page = '/welcome';
  }

  \Drupal::configFactory()
    ->getEditable('system.site')
    ->set('page.front', $front_page)
    ->save(TRUE);
}

/**
 * Implements hook_install_tasks_alter().
 */
function cloud_orchestrator_install_tasks_alter(array &$tasks, array $install_state) {
  $tasks['install_finished']['function'] = 'cloud_orchestrator_after_install_finished';
}

/**
 * Redirect to cloud_config add form after installer finishes.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   A renderable array with a redirect header.
 */
function cloud_orchestrator_after_install_finished(array &$install_state) {
  global $base_url;
  $add_cloud_config_url = $base_url . '/?tour';
  install_finished($install_state);

  $output = [];

  // Clear all messages.
  \Drupal::messenger()->deleteAll();

  $output = [
    '#title' => t('Cloud Orchestrator'),
    'info' => [
      '#markup' => t('<p>Congratulations, you have installed Cloud Orchestrator!</p><p>If you are not redirected to the front page in 5 seconds, Please <a href="@url">click here</a> to proceed to your installed site.</p>', [
        '@url' => $add_cloud_config_url,
      ]),
    ],
    '#attached' => [
      'http_header' => [
        ['Cache-Control', 'no-cache'],
      ],
    ],
  ];

  $meta_redirect = [
    '#tag' => 'meta',
    '#attributes' => [
      'http-equiv' => 'refresh',
      'content' => '0;url=' . $add_cloud_config_url,
    ],
  ];
  $output['#attached']['html_head'][] = [$meta_redirect, 'meta_redirect'];

  return $output;
}

/**
 * Utility function to update yml files.
 *
 * @param array $files
 *   Array of yml files.
 * @param string $dir
 *   Directory where yml files are found.
 */
function cloud_orchestrator_update_yml_definitions(array $files, $dir) {
  $config_manager = \Drupal::service('config.manager');
  $config_path = realpath(drupal_get_path('profile', 'cloud_orchestrator')) . '/config/' . $dir;

  foreach ($files as $file) {
    $filename = $config_path . '/' . $file;
    $file = file_get_contents($filename);
    if (!$file) {
      continue;
    }
    $value = Yaml::decode($file);
    $type = $config_manager->getEntityTypeIdByName(basename($filename));
    $entity_manager = $config_manager->getEntityTypeManager();
    $definition = $entity_manager->getDefinition($type);
    $id_key = $definition->getKey('id');
    $id = $value[$id_key];
    $entity_storage = $entity_manager->getStorage($type);
    $entity = $entity_storage->load($id);
    if ($entity) {
      $entity = $entity_storage->updateFromStorageRecord($entity, $value);
      $entity->save();
    }
    else {
      $entity = $entity_storage->createFromStorageRecord($value);
      $entity->save();
    }
  }
}

/**
 * Implements template_preprocess_layout().
 */
function cloud_orchestrator_preprocess_layout(&$variables) {
  // Set the tab_labels for the twig template.
  $variables['tab_labels'] = isset($variables['content']['#tab_labels']) ? $variables['content']['#tab_labels'] : [];
}
